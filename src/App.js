import logo from "./logo.svg";
import "./App.css";
import { dummyJson, dummyJson2 } from "./dummyJson";

function formatJSON(jsonString) {
  try {
    const jsonObject = JSON.parse(jsonString);
    const formattedJSON = JSON.stringify(jsonObject, null, 8);
    return formattedJSON;
  } catch (error) {
    return `Error formatting JSON: ${error.message}`;
  }
}

const identationString = "\xa0 \xa0 ";

const jsonString =
  '{"id":1,"title":"iPhone 9","description":"An apple mobile which is nothing like apple","price":549,"discountPercentage":12.96,"rating":4.69,"stock":94,"brand":"Apple","category":"smartphones","thumbnail":"https://i.dummyjson.com/data/products/1/thumbnail.jpg","images":["https://i.dummyjson.com/data/products/1/1.jpg","https://i.dummyjson.com/data/products/1/2.jpg","https://i.dummyjson.com/data/products/1/3.jpg","https://i.dummyjson.com/data/products/1/4.jpg","https://i.dummyjson.com/data/products/1/thumbnail.jpg"]}';

const getRow = (key, value, identation) => {
  let str = identationString.repeat(identation);

  // let str = identation;
  if (
    typeof value === "number" ||
    typeof value === "string" ||
    typeof value === "null" ||
    typeof value === "undefined" ||
    typeof value === "boolean" ||
    typeof value === "bigint"
  ) {
    return <div className="row">{`${str}"${key}": "${value}",`}</div>;
  }
  if (Array.isArray(value)) {
    let midEl = value.map((arrEl) => {
      if (
        typeof arrEl === "number" ||
        typeof arrEl === "string" ||
        typeof arrEl === "null" ||
        typeof arrEl === "undefined" ||
        typeof arrEl === "boolean" ||
        typeof arrEl === "bigint"
      ) {
        return (
          <div className="row">
            {str + identationString}"{arrEl}",
          </div>
        );
      } else {
        // if it is array of object
        return [
          <div className="row">
            {str + identationString}
            {"{"}
          </div>,
          Object.keys(arrEl).map((key) =>
            getRow(key, arrEl[key], identation + 2)
          ),
          <div className="row">
            {str + identationString}
            {"}"}
          </div>,
        ];
      }
    });

    return [
      <div className="row">
        {str}
        {"["}
      </div>,
      midEl,
      <div className="row">
        {str}
        {"]"},
      </div>,
    ];
  }
  return [
    <div className="row">
      {str + identationString}
      {"{"}
    </div>,
    Object.keys(value).map((key) => getRow(key, value[key], identation + 2)),
    <div className="row">
      {str + identationString}
      {"}"}
    </div>,
  ];
};

const getFormattedHTML = (jsonObj) => {
  if (Array.isArray(jsonObj)) {
    return [getRow(null, jsonObj, 0)];
  } else {
    return [
      <div className="row">{"{"}</div>,
      Object.keys(jsonObj).map((key) => getRow(key, jsonObj[key], 1)),
      <div className="row">{"}"},</div>,
    ];
  }
};

function App() {
  // Call the formatJSON function to format the JSON
  // const formattedJSON = formatJSON(jsonString);

  const jsonObject = JSON.parse(dummyJson);

  return (
    <div className="App">
      JSON COMPARATOR
      <br />
      <br />
      <div className="wrapper">
        <div className="code-block">{getFormattedHTML(jsonObject)}</div>
        <div className="code-block">{getFormattedHTML(jsonObject)}</div>
      </div>
    </div>
  );
}

export default App;
